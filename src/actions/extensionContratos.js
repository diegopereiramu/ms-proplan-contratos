
import { registrar } from  '@repository/extensionContratos';

const nuevaExtension = async (req, res, next) => {

    let extensionCreada = await registrar(req.body);
    console.log(extensionCreada);
    res.status(200).json({
        OK:true, 
        data: extensionCreada,
    });      
};

export {
    nuevaExtension
};