
import { obtenerPorIdContrato,  obtenerPorCodigoContrato} from  '@repository/contrato';
import { obtenerCentrosCostoPorIdContrato } from  '@repository/centroCosto';
import { mapToContratos } from  '@helpers/contratos';

const obtenerContratosPorId = async (req, res, next) => {

    let response = null;
    const contratos = await obtenerPorIdContrato(req.params.id, req.query.idPeriodo);

    if(contratos.length > 0) {
        const idContratos = contratos.map(contrato => contrato.id);

        const centrosDeCosto = await obtenerCentrosCostoPorIdContrato(idContratos);
        response = mapToContratos(contratos, centrosDeCosto, true);
    }

    res.status(200).json({
        OK:true, 
        data: response,
    });      
};

const obtenerContratosPorCodigo = async (req, res, next) => {

    let response = null;
    const contratos = await obtenerPorCodigoContrato(req.params.codigo, req.query.idPeriodo);

    if(contratos.length > 0) {
        const idContratos = contratos.map(contrato => contrato.id);

        const centrosDeCosto = await obtenerCentrosCostoPorIdContrato(idContratos);
        response = mapToContratos(contratos, centrosDeCosto, true);
    }

    res.status(200).json({
        OK:true, 
        data: response,
    });      
};


export {
    obtenerContratosPorId,
    obtenerContratosPorCodigo,
};