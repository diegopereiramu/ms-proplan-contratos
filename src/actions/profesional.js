import { obtenerPorIdProfesional } from  '@repository/contrato';
import { obtenerCentrosCostoPorIdContrato } from  '@repository/centroCosto';
import { obtenerExtensionesPorIdContrato } from  '@repository/extensionContratos'
import { mapToContratos } from  '@helpers/contratos';

const obtenerContratosPorIdProfesional = async (req, res, next) => {

    let response = null;
    const contratos = await obtenerPorIdProfesional(req.params.id, req.query.idPeriodo);
    // console.log(contratos)
    if(contratos.length > 0) {
        const idContratos = contratos.map(contrato => contrato.id);
        const extensionDeContratos = await obtenerExtensionesPorIdContrato(idContratos);
        console.log(extensionDeContratos)

        const centrosDeCosto = await obtenerCentrosCostoPorIdContrato(idContratos);
        response = mapToContratos(contratos,extensionDeContratos, centrosDeCosto);
    }

    res.status(200).json({
        OK:true, 
        data: response,
    });      
};


export {
    obtenerContratosPorIdProfesional,
};