import { query } from './pg.connect';

const obtenerCentrosCostoPorIdContrato = async (idContratos) => {
    const result = await query(` SELECT 
            ccc.contrato_id,
            cc.id,
            cc.codigo,
            cc.descripcion
        FROM "proplan"."contrato_centro_costo" ccc
        INNER JOIN "proplan"."centro_costo" cc ON cc.id = ccc.centro_costo_id
        WHERE ccc.contrato_id = ANY ($1::int[]);`, [idContratos]);

    return result.rows;
};

export {
    obtenerCentrosCostoPorIdContrato, 
}
