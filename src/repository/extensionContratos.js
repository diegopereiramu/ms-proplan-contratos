import { query } from './pg.connect';
const moment = require('moment');

const obtenerExtensionesPorIdContrato = async (idContratos) => {
    try {
        const result = await query(`SELECT 
        id, 
        contrato_id, 
        horas_semanales, 
        dias_feriados_legales, 
        dias_descanso_compensatorio,
        dias_permiso_administrativo,
        dias_congreso_capacitacion,
        tiempo_lactancia_semanal,
        tiempo_colacion_semanal,
        sistema_turno,
        fecha_inicio,
        fecha_termino,
        fecha_creacion_registro,
        activo 
        FROM "proplan"."contrato_extension" 
        WHERE contrato_id = ANY ($1::int[])
        ORDER BY id ASC;`, [idContratos]);
        return result.rows;
    } catch(error){
        throw(error);
    }

};

const registrar = async (req) => {

    try {
        const q = `
        INSERT INTO "proplan"."contrato_extension" 
            (contrato_id,
            horas_semanales, 
            dias_feriados_legales, 
            dias_descanso_compensatorio, 
            dias_permiso_administrativo, 
            dias_congreso_capacitacion, 
            tiempo_lactancia_semanal, 
            tiempo_colacion_semanal, 
            sistema_turno, 
            fecha_inicio,
            fecha_termino) 
        VALUES (
            ${req.idContrato},
            ${req.horas_semanales},
            ${req.feriados_legales},
            ${req.descanso_compensatorio},
            ${req.permiso_administrativo},
            ${req.congreso_capacitacion},
            ${req.lactancia_semanal},
            ${req.colacion_semanal},
            ${req.sistema_turno},
            '${req.fecha_inicio}',
            '${req.fecha_termino}'
        ) RETURNING id;`
        const result = await query(q);
        const updateDate = await updateExtension(req.idContrato, req.fecha_inicio, result.rows[0].id);

        return result.rows[0].id;
    } catch (error) {

        throw error;
    }
};

const updateExtension = async (contrato, fechaInicio, newId) => {
    try {
        const q = `UPDATE "proplan"."contrato_extension" SET activo = false, fecha_termino = $1
        where id IN (
        select * from
          (
            select id
            from "proplan"."contrato_extension"
            WHERE contrato_id = (${contrato}) AND id != ${newId} AND activo = true
          ) as id);`
          const result = await query(q, [
            moment(fechaInicio).subtract(1,'d').format('YYYY-MM-DD')
        ]);
        console.log(result);
        // return result.insertId;
    } catch (error) {

        throw error;
    }
};
export {
    obtenerExtensionesPorIdContrato,
    registrar
}
