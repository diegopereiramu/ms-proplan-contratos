import { query } from './pg.connect';

const obtenerPorIdProfesional = async (id, idPeriodo) => {
    const result = await query(`
        SELECT 
            c.id,
            c.profesional_id,
            c.ley_id,
            c.codigo,
            c.especialidad_sis,
            c.fecha_inicio,
            c.fecha_termino,
            c.fecha_creacion_registro,
            c.activo,
            l.codigo AS ley_codigo,
            l.descripcion AS ley_descripcion,
            l.distribucion_horas
        FROM "proplan"."contrato" c
        INNER JOIN 
            "proplan"."ley" l ON l.id = c.ley_id
        ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
            "proplan"."periodo" pe on pe.id = $1 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
        WHERE 
            c.profesional_id = $2;`, [idPeriodo, id]);

    return result.rows;
};

const obtenerPorIdContrato = async (id, idPeriodo) => {
    const q = `
    SELECT
        c.id,
        c.profesional_id,
        c.ley_id,
        c.codigo,
        c.especialidad_sis,
        c.horas_semanales,
        c.dias_feriados_legales,
        c.dias_descanso_compensatorio,
        c.dias_permiso_administrativo,
        c.dias_congreso_capacitacion,
        c.tiempo_lactancia_semanal,
        c.tiempo_colacion_semanal,
        c.sistema_turno,
        c.fecha_inicio,
        c.fecha_termino,
        c.fecha_creacion_registro,
        c.activo,
        l.codigo AS ley_codigo,
        l.descripcion AS ley_descripcion,
        l.distribucion_horas
    FROM "proplan"."contrato" c
    INNER JOIN 
        "proplan"."ley" l ON l.id = c.ley_id
    ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
        "proplan"."periodo" pe on pe.id = $1 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
    WHERE 
        c.id = $2;`;
    const result = await query(q, [idPeriodo, id]);

    return result.rows;
};

const obtenerPorCodigoContrato = async (codigo, idPeriodo) => {
    const result = await query(`
    SELECT
        c.id,
        c.profesional_id,
        c.ley_id,
        c.codigo,
        c.especialidad_sis,
        c.horas_semanales,
        c.dias_feriados_legales,
        c.dias_descanso_compensatorio,
        c.dias_permiso_administrativo,
        c.dias_congreso_capacitacion,
        c.tiempo_lactancia_semanal,
        c.tiempo_colacion_semanal,
        c.sistema_turno,
        c.fecha_inicio,
        c.fecha_termino,
        c.fecha_creacion_registro,
        c.activo,
        l.codigo AS ley_codigo,
        l.descripcion AS ley_descripcion,
        l.distribucion_horas
    FROM "proplan"."contrato" c
    INNER JOIN 
        "proplan"."ley" l ON l.id = c.ley_id
    ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
        "proplan"."periodo" pe on pe.id = $1 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
    WHERE 
        c.codigo = $2;`, [idPeriodo, codigo]);

    return result.rows;
};

export {
    obtenerPorIdProfesional,
    obtenerPorIdContrato,
    obtenerPorCodigoContrato,
}
