import moment from 'moment'

const mapToContratos = (contratos, extensionContratos, centrosDeCosto, soloUnContrato = false) => {

    let returnData = [];
    let actualCentrosCosto;
    let actualExtensionContrato;
    contratos.forEach(contrato => {
        actualCentrosCosto = centrosDeCosto.filter(centroCosto =>
            centroCosto.contrato_id == contrato.id
        ).map(centroCosto => {
            return {
                id: centroCosto.id,
                codigo: centroCosto.codigo,
                descripcion: centroCosto.descripcion
            }
        })
        actualExtensionContrato = extensionContratos.filter(extensionContratos =>
            extensionContratos.contrato_id == contrato.id).map(extensionContratos => {
                return {
                    id: extensionContratos.id,
                    horas_semanales: parseFloat(extensionContratos.horas_semanales),
                    dias: {
                        feriados_legales: extensionContratos.dias_feriados_legales,
                        descanco_compensatorio: extensionContratos.dias_descanso_compensatorio,
                        permiso_administrativo: extensionContratos.dias_permiso_administrativo,
                        congreso_capacitacion: extensionContratos.dias_congreso_capacitacion
                    },
                    tiempo: {
                        lactancia_semanal: extensionContratos.tiempo_lactancia_semanal,
                        colacion_semanal: extensionContratos.tiempo_colacion_semanal
                    },
                    sistema_turno: extensionContratos.sistema_turno,
                    fecha_inicio: moment(extensionContratos.fecha_inicio).format('DD-MM-YYYY'),
                    fecha_termino: moment(extensionContratos.fecha_termino).format('DD-MM-YYYY'),
                    fecha_creacion_Registro: moment(extensionContratos.fecha_creacion_registro).format('DD-MM-YYYY'),
                    activo: extensionContratos.activo,
                }
            })
        returnData.push(mapToContrato(contrato, actualExtensionContrato, actualCentrosCosto));
    })

    if (soloUnContrato) {
        returnData = returnData[0];
    }

    return returnData;
}

const mapToContrato = (contrato, extensionContrato, centrosCosto) => {
    return {
        id: contrato.id,
        codigo: contrato.codigo,
        profesional_id: contrato.profesional_id,

        extensiones: extensionContrato,
        ley: {
            id: contrato.ley_id,
            codigo: contrato.ley_codigo,
            descripcion: contrato.ley_descripcion,
            distribucion_horas: contrato.distribucion_horas,
        },
        inicio: moment(contrato.fecha_inicio).format('DD-MM-YYYY'),
        termino: contrato.fecha_termino != null ? moment(contrato.fecha_termino).format('DD-MM-YYYY') : null,
        activo: contrato.activo,
        centro_costos: centrosCosto,
        especialidad_sis: contrato.especialidad_sis,
    }
}

export {
    mapToContratos,
}