import { index, testDb } from '@actions/home';
import { obtenerContratosPorIdProfesional } from '@actions/profesional';
import { obtenerContratosPorId, obtenerContratosPorCodigo } from '@actions/contratos';
import { nuevaExtension } from '@actions/extensionContratos';


export default (router, app) => {
    router.get('/', index);
    router.get('/:id', obtenerContratosPorId);
    router.get('/codigo/:codigo', obtenerContratosPorCodigo);
    router.get('/profesional/:id', obtenerContratosPorIdProfesional);
    router.post('/nueva-extension', nuevaExtension);
    router.get('/test-db', testDb);
};