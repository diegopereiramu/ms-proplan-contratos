# Microservicio Contratos

## Clonar

```sh
$ git clone https://bitbucket.org/amarischile/ms-proplan-contratos.git
$ cd ms-proplan-contratos
$ cp .env .env.local
$ npm install
$ npm test
$ npm run start:local
```
> Debes configurar las variables de entorno para tu desarrollo local en .env.local


## Docker

```sh
$ docker build -t ms-proplan-contratos .

$ docker run -it \
-e APP_PORT=1002 \
-e APP_HOST=0.0.0.0 \
-e APP_PREFIX=/ \
-e APP_HEALTH_PATH=/health \
-e MYSQL_CONNECTION_STRING="mysql://user:pass@server/dbname?debug=true&charset=UTF8_GENERAL_CI" \
-p 1002:1000 --name ms-proplan-contratos ms-proplan-contratos
```

## Verificar

Verificar que el servicio este arriba

```sh
$ curl -v http://localhost:1002/health
```